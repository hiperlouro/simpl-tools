"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appRouter = void 0;
const express_1 = require("express");
exports.appRouter = express_1.Router();
function route(options) {
    return (target, propertyKey, descriptor) => {
        exports.appRouter[options.method](options.path, target[propertyKey]);
    };
}
exports.default = route;
//# sourceMappingURL=routes.decorator.js.map