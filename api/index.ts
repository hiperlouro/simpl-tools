import APIController from "./controller/api.controller";
import route from './routes.decorator';

export const Route = route;

export default APIController
