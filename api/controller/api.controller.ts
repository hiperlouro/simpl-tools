import express from 'express';
import { appRouter } from '../routes.decorator';
import {serve, setup} from 'swagger-ui-express';

class APIController {
    app = express();
    port: number = 4000;
    callbackFns: any;

    constructor(props: any) {
        this.port = props.port;
        this.app.use(appRouter);
        if (props.swaggerDocument) {
            this.app.use('/swagger', serve, setup(props.swaggerDocument))
        }
        this.app.listen(this.port, () => {
            console.log(`${props.name} listening at http://localhost:${this.port}`)
        });
    }
}

export default APIController;
