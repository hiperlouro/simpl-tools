"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const routes_decorator_1 = require("../routes.decorator");
const swagger_ui_express_1 = require("swagger-ui-express");
class APIController {
    constructor(props) {
        this.app = express_1.default();
        this.port = 4000;
        this.port = props.port;
        this.app.use(routes_decorator_1.appRouter);
        if (props.swaggerDocument) {
            this.app.use('/swagger', swagger_ui_express_1.serve, swagger_ui_express_1.setup(props.swaggerDocument));
        }
        this.app.listen(this.port, () => {
            console.log(`${props.name} listening at http://localhost:${this.port}`);
        });
    }
}
exports.default = APIController;
//# sourceMappingURL=api.controller.js.map