"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Route = void 0;
const api_controller_1 = __importDefault(require("./controller/api.controller"));
const routes_decorator_1 = __importDefault(require("./routes.decorator"));
exports.Route = routes_decorator_1.default;
exports.default = api_controller_1.default;
//# sourceMappingURL=index.js.map